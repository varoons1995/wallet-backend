'use strict'


const Logger = use('Logger')
const Database = use('Database')


class UserController {
    // creating and saving a new user (sign-up)
    async register ({ request, auth, response }) {
      try {
        // getting data passed within the request
        const data = request.only(['username', 'password'])
        // looking for user in database

        const userExists1 = await User.findBy('phone', data.phone)
        // if user exists don't save
        if (userExists1) {
          return response
            .status(400)
            .send({ message: { error: 'User already registered' } })
        }
        // if user doesn't exist, proceeds with saving him in DB
        const user = await User.create(data)
        //Generate Token
        let token = await auth.generate(user)
        Object.assign(user, token)
        console.log(user)
        Logger
        .transport('file')
        .info(new Date().toLocaleString()+' --- Create Id : '+user.$attributes.id)
  
        return response.json(user)
      } catch (err) {
        return response
          .status(err.status)
          .send(err)
      }
    }

    async login({request, auth, response}) {
      const query = Database.table('STAFF')
      console.log(request.all())
      let {USERNAME, PASSWORD} = request.all();
      let sd = await query.where('USERNAME',USERNAME)
      let cd = await auth.attempt(USERNAME, PASSWORD)
      let token = await auth.attempt(USERNAME,PASSWORD)
      console.log(token)
      try {
        if (await auth.attempt(username, password)) {
          let user = await USER.findBy('username', username)
          let token = await auth.attempt(user)
          console.log(token)
          return response.json(user)
        }
      }
      catch (e) {
        console.log(e)
        return response.json({message: 'You are not registered!'})
      }
    }
    async forgot ({ request, response }) {
      return  { message: 'Forgot Password Route' }
    }

    async getPosts({request, response ,param}) {
    //  let posts = await Post.query().with('user').fetch()

      return { Error: '404 Not Found' }
    }

  }

module.exports = UserController