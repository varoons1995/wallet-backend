'use strict'

const Database = use('Database')

class StaffClassController {

    async FindAll(){
        const query = Database.table('STAFFCLASS')
        const StaffClassAll = await query
        return StaffClassAll;
    }
}

module.exports = StaffClassController
