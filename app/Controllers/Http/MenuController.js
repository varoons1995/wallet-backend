'use strict'

const Database = use('Database')


class MenuController {

    async FindAll(){
        const query = Database.table('MENU')

        const MenuAll = await query
        console.log(MenuAll)
        return MenuAll;
    }

    async addMenu(request,response){
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        const query = Database.table('MENU')
        
        const body = request._request_._original;
        console.log(body)
        const addMenu = await query.insert({
            'MENUNAME':body.MENUNAME,
            'MENUCODE':body.MENUCODE,
            'CREATEDATE':dt
        })
        if(addMenu){
            response = addMenu[0]
        }else{
            response = 'Error'
        }
        
        return {message : response}
    }


}

module.exports = MenuController
