'use strict'
const Database = use('Database')

class HomeController {

    async Reportall(request , response){
        const body = request._request_._original
        let tr_fdate = body.CREATEDATE
        let tr_edate = body.ENDDATE
        
        const bank_owner = await Database.raw("SELECT  BO.ID, BO.BANK_TITLE, BO. BANK_ACCOUNT_NUMBER, BO.BANK_ACCOUNT_NAME, IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND DATE_FORMAT(TD.UPDATEDATE ,'%Y-%m-%d') = DATE_FORMAT(DATE_ADD(SYSDATE(), INTERVAL 7 HOUR),'%Y-%m-%d') AND BO.ID = TD.BANK_OWNER_ID),0) AS SUM_TODAY, DATE_FORMAT((SELECT MAX(TD.UPDATEDATE ) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE BO.ID = TD.BANK_OWNER_ID AND TD.TRANSACTION_STATUS = 'success'),'%d-%m-%Y %T') AS CREATEDATE FROM UFABET.BANK_OWNER BO WHERE  BO.BANK_STATUS = 'Y' AND BO.BANK_TYPE = 'deposit' GROUP BY BO.BANK_ACCOUNT_NUMBER");
        const trxde_order_desc = await Database.raw("SELECT TD.ID AS TRANSACTION_DEPOSIT_ID, M.MEMBER_USERNAME, TD.BALANCE, TD.TRANSACTION_GEAR,B.BANK_TITLE ,TD.ACTION_BY, M.CREDITE AS F_CREDITE, (M.CREDITE + TD.BALANCE) AS L_CREDITE, DATE_FORMAT(TD.UPDATEDATE ,'%d/%m/%Y %T') AS CREATEDATE FROM UFABET.TRANSACTION_DEPOSIT  TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER B ON TD.BANK_OWNER_ID = B.ID WHERE TD.TRANSACTION_STATUS = 'success' AND DATE_FORMAT(TD.UPDATEDATE ,'%Y-%m-%d') = DATE_FORMAT(SYSDATE(),'%Y-%m-%d') ORDER BY TD.UPDATEDATE DESC");
        const trxde_new = await Database.raw("SELECT TD.ID AS TRANSACTION_DEPOSIT_ID, TD.CREATEDATE,B.BANK_TITLE  ,TD.BALANCE FROM UFABET.TRANSACTION_DEPOSIT TD LEFT JOIN UFABET.MEMBER M ON TD.MEMBER_ID = M.ID LEFT JOIN UFABET.BANK_OWNER B ON TD.BANK_OWNER_ID = B.ID WHERE TD.TRANSACTION_STATUS = 'pending'  ORDER BY TD.CREATEDATE DESC");
        // console.log(bank_owner[0])
        // console.log(trxde_order_desc[0])
        // console.log(trxde_new[0])
        return {
            status : 200,
            bank_owner : bank_owner[0],   
            trxde_order_desc : trxde_order_desc[0],
            trxde_new : trxde_new[0]
        }
    }
}

module.exports = HomeController
