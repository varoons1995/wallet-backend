'use strict'

const Staff = use("App/Models/Staff")

const Database = use('Database')

class StaffController {

    async FindAll(){
        const StaffAll = await Database.raw('SELECT S.ID, SC.CLASSNAME, SC.CLASSCODE,SC.ID AS STAFFCLASS_ID, S.USERNAME, S.PASSWORD, S.STAFFNAME, S.DEPOSITLIMIT, S.CREATEDATE, S.LASTUPDATE, S.STATUS_ONLINE, S.ACTIVE  FROM UFABET.STAFF S LEFT JOIN UFABET.STAFFCLASS SC ON S.STAFFCLASSID = SC.ID')
        console.log(StaffAll[0])
        return StaffAll[0];
    }

    async addStaff(request,response){
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        const query = Database.table('STAFF')
        const body = request._request_._original;
        let as = '200'
        let sd = 'Message Ba'
        console.log(body)
        let sql_staff = await query.where('USERNAME', body.USERNAME).first()
        console.log(sql_staff)
        if(sql_staff){
            as = '300',
            sd = 'Staff_Username Already !!!'
        }else{
            try{
                const addStaff = await query.insert({
                    'USERNAME':body.USERNAME,
                    'PASSWORD':body.PASSWORD,
                    'STAFFNAME':body.STAFFNAME,
                    'DEPOSITLIMIT':body.DEPOSITLIMIT,
                    'STAFFCLASSID':body.STAFFCLASSID,
                    'CREATEDATE':dt,
                    'LASTUPDATE':dt,
                    'STATUS_ONLINE':body.STATUS_ONLINE,
                    'ACTIVE':body.ACTIVE
                })
                if(addStaff){
                    as = '200'
                    sd = 'Suscess'
                }else{
                    as = '302'
                    sa = 'Error Server'
                }
            }
            catch(err){
                console.error(err)
            }

        }
        return {status : sd,
                message : as}
    }


    async EditStaff(request,response){
        const query = Database.table('STAFF')
        const body = request._request_._original;
        console.log(body)
        const EditStaff = await query.where('ID',body.ID)
        .update({
            'STAFFNAME':body.STAFFNAME,
            'STAFFCLASSID':body.STAFFCLASSID,
            'ACTIVE': body.STATUS,
            'DEPOSITLIMIT':body.DEPOSITLIMIT
        })
        console.log(EditStaff)
        if(EditStaff > 0){
            response = 'Suscess'
        }else{
            response = 'Error'
        }
    
        return {message : response}
    }

    async login({ request,auth,response}) {
        const { username, password } = request.all()
        const query = Database.table('STAFF')
        const token = Database.table('tokens')
        let v = await Staff.query().where('USERNAME',username).where('PASSWORD' , password).first()
        // if check login
        let sd
        let statusLogin
        
        if(v){
            if(v.ACTIVE == 'Y'){
            console.log(v.ID)
            let a = await Database.raw('SELECT * FROM UFABET.tokens WHERE staff_id = ? ORDER BY id DESC LIMIT 1;',[v.ID])
            console.log(a[0][0])
            let b = await token.where('id' ,a[0][0].id).update({ is_revoked: 1})
            sd = await auth.generate(v)
            statusLogin = 200
            }else{
                sd = 'Staff Status Inactive'
                statusLogin = 400
            }
        }else{
             sd = 'invalid username or password'
             statusLogin = 400
        }
        return {
                status : statusLogin,
                Token : sd,
                Profile : v 
               }
    }

}

module.exports = StaffController
