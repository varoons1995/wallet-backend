'use strict'

const Database = use('Database')
const axios = require('axios')

class CreditController {

    async FindAll(){
        const query = await Database.raw('SELECT TC.ID, TC.MEMBER_ID, M.MEMBER_USERNAME, TC.TRANSACTION_TYPE, TC.CREDIT, TC.REMARK, TC.CREATEDATE, TC.CREATE_BY FROM UFABET.TRANSACTION_CREDIT TC INNER JOIN UFABET.MEMBER M ON M.ID =  TC.MEMBER_ID ORDER BY TC.ID DESC')
        return query[0];
    }
    
    async addCreditAPI(request, response){
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        const query = Database.table('TRANSACTION_CREDIT')
        const member = Database.table('MEMBER')
        const body = request._request_._original
        let sd = 'Message Backend'
        let as = '200'
        console.log(body)	
         let getMember = await member.where('ID',body.MEMBER_ID).first()
         console.log(getMember)
            const queryWeb = Database.table('WEBSITE')
            const webAgent = await queryWeb.where('ID',getMember.WEBSITE_ID).first();
            console.log(webAgent)
            try{
              //Deposit
              if(body.TRANSACTION_TYPE == 'deposit'){
                await axios({
                  method: 'get',
                  url: 'http://apibot.ufatool.com/bot',
                  params : 
                    { method :'requesttoken',
                      agenusername : webAgent.USERNAME,
                      ufausername : getMember.MEMBER_USERNAME,
                      logmethod : 'addcredit'
                    }
                })
                  .then(response => {
                    console.log(response.data)
                    if(response.data.status == 'error'){
                      as = '301',
                      sd = 'UFABET API ERROR'
                    }else{
                        console.log("response: ", response.data)
                        var token = response.data.token ;
                     axios({
                      method: 'get',
                      url: 'http://apibot.ufatool.com/bot',
                      params : 
                        { method :'addcredit',
                          agenusername : webAgent.USERNAME,
                          agenpassword : webAgent.PASSWORD,
                          ufausername : getMember.MEMBER_USERNAME,
                          credit : body.CREDIT,
                          token : token,
                        }
                    })
                      .then(response => {
                        console.log("response: ", response)
                        // do something about response
                        console.log(response.data)
                        if(response.data.status == 'error'){
                          as = '301',
                          sd = 'UFABET API ERROR'
                        }else{
                          as = '200'
                          sd = 'Add Credit in UFABET Success'
                        }
                      })
                      .catch(err => {
                        console.error(err)
                      })
                     }
                    // do something about response
                  })
                  .catch(err => {
                    console.error(err)
                  })
                  // Withdraw
                  }else{
                    await axios({
                      method: 'get',
                      url: 'http://apibot.ufatool.com/bot',
                      params : 
                        { method :'requesttoken',
                          agenusername : webAgent.USERNAME,
                          ufausername : getMember.MEMBER_USERNAME,
                          logmethod : 'delcredit'
                        }
                    })
                      .then(response => {
                        console.log(response.data)
                        if(response.data.status == 'error'){
                          as = '301',
                          sd = 'UFABET API ERROR'
                        }else{
                            console.log("response: ", response.data)
                            var token = response.data.token ;
                         axios({
                          method: 'get',
                          url: 'http://apibot.ufatool.com/bot',
                          params : 
                            { method :'delcredit',
                              agenusername : webAgent.USERNAME,
                              agenpassword : webAgent.PASSWORD,
                              ufausername : getMember.MEMBER_USERNAME,
                              credit : body.CREDIT,
                              token : token,
                            }
                        })
                          .then(response => {
                            console.log("response: ", response)
                            // do something about response
                            console.log(response.data)
                            if(response.data.status == 'error'){
                              as = '301',
                              sd = 'UFABET API ERROR'
                            }else{
                              as = '200'
                              sd = 'Add Credit in UFABET Success'
                            }
                          })
                          .catch(err => {
                            console.error(err)
                          })
                         }
                        // do something about response
                      })
                      .catch(err => {
                        console.error(err)
                      })
                  }
                } catch(err) {
                  console.error(err)
                }

              //  as = '200'
        if(as == '200')   {   
        console.log(dt)
        const addCredit = await query.insert({
            'MEMBER_ID':getMember.ID,
            'TRANSACTION_TYPE':body.TRANSACTION_TYPE,
            'CREDIT':body.CREDIT,
            'REMARK':body.REMARK,
            'CREATEDATE':dt,
            'CREATE_BY':body.CREATEBY,
        })
        if(addCredit){
            as ='200'
            sd = 'Add Credit Success'
        }else{
            as = '302'
            sd = 'Error Server'
        }

    }
    
    return {status :  as,
            message :  sd};
    }

  // async addCredit(request, response){
  //     var dt = new Date();
  //     dt.setHours(dt.getHours()+7);
  //     const query = Database.table('TRANSACTION_CREDIT')
  //     const body = request._request_._original
  //     console.log(body)	

  //     try{
  //         const addCredit = await query.insert({
  //             'MEMBER_ID':body.MEMBER_ID,
  //             'TRANSACTION_TYPE':body.TRANSACTION_TYPE,
  //             'CREDIT':body.CREDIT,
  //             'REMARK':body.REMARK,
  //             'CREATEDATE':dt,
  //             'CREATE_BY':body.CREATE_BY
  //         })
  //         if(addCredit){
  //             response = 'Success'
  //         }else{
  //             response = 'Error'
  //         }            
  //         return {message : response}

  //     }catch(err){
  //         console.error(err)
  //     }
      
  // }

  async editCredit(request, response){
      const query = Database.table('TRANSACTION_CREDIT')
      const body = request._request_._original
      console.log(body)	
      var dt = new Date();
      dt.setHours(dt.getHours()+7);
      try{
          const editCredit = await query.where('ID',body.ID)
          .update({
              'MEMBER_ID':body.MEMBER_ID,
              'TRANSACTION_TYPE':body.TRANSACTION_TYPE,
              'CREDIT':body.CREDIT,
              'REMARK':body.REMARK,
              'CREATEDATE':dt,
              'CREATE_BY':body.CREATE_BY
          })
          if(editCredit){
              response = 'Success'
          }else{
              response = 'Error'
          }            
          return {message : response}

      }catch(err){
          console.error(err)
      }
      
  }

  async findByCreateDate(request , response){
    const body = request._request_._original
    let tr_fdate = body.CREATEDATE
    let tr_edate = body.ENDDATE
    let query
    let status
    let message
    if(body.USERNAME !=''){
      const query = await Database.raw("SELECT TC.ID AS TRANSACTION_CREDIT_ID ,TC.TRANSACTION_TYPE ,M.MEMBER_USERNAME, TC.CREDIT, TC.CREATEDATE , TC.CREATE_BY,TC.REMARK FROM UFABET.TRANSACTION_CREDIT TC LEFT JOIN UFABET.MEMBER M ON  TC.MEMBER_ID = M.ID  WHERE TC.CREATEDATE BETWEEN ? AND ? AND M.MEMBER_USERNAME = ? ORDER BY TC.CREATEDATE DESC",[tr_fdate,tr_edate ,body.USERNAME]);
        status = 200
        message = query[0]
    }else{
      if(body.AGENTTYPE !=''){
        const query = await Database.raw("SELECT TC.ID AS TRANSACTION_CREDIT_ID ,TC.TRANSACTION_TYPE ,M.MEMBER_USERNAME, TC.CREDIT, TC.CREATEDATE , TC.CREATE_BY,TC.REMARK FROM UFABET.TRANSACTION_CREDIT TC LEFT JOIN UFABET.MEMBER M ON  TC.MEMBER_ID = M.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID  WHERE TC.CREATEDATE BETWEEN ? AND ? AND W.WEBSITENAME = ? ORDER BY TC.CREATEDATE DESC",[tr_fdate,tr_edate ,body.AGENTTYPE]);
        status = 200
        message = query[0]
      }else{
      const query = await Database.raw("SELECT TC.ID AS TRANSACTION_CREDIT_ID ,TC.TRANSACTION_TYPE ,M.MEMBER_USERNAME, TC.CREDIT, TC.CREATEDATE , TC.CREATE_BY,TC.REMARK FROM UFABET.TRANSACTION_CREDIT TC LEFT JOIN UFABET.MEMBER M ON  TC.MEMBER_ID = M.ID  WHERE TC.CREATEDATE BETWEEN ? AND ? ORDER BY TC.CREATEDATE DESC",[tr_fdate,tr_edate]);
      status = 200
      message = query[0]
      }
    }
    return {status : status,
            message : message}
  }

  async sumCredit(request , response){
    const body = request._request_._original
    let tr_fdate = body.CREATEDATE
    let tr_edate = body.ENDDATE
    let query
    let result
    let status
    let message1
    let message2
      query = await Database.raw("SELECT IFNULL((SELECT SUM(TC.CREDIT) FROM UFABET.TRANSACTION_CREDIT TC WHERE  TC.CREATEDATE BETWEEN ? AND ? AND TC.TRANSACTION_TYPE = 'deposit'),0) AS DEPOSIT, IFNULL((SELECT SUM(TC.CREDIT) FROM UFABET.TRANSACTION_CREDIT TC WHERE  TC.CREATEDATE BETWEEN ? AND ? AND TC.TRANSACTION_TYPE = 'withdraw'),0) AS WITHDRAW, IFNULL(((SELECT SUM(TC.CREDIT) FROM UFABET.TRANSACTION_CREDIT TC WHERE  TC.CREATEDATE BETWEEN ? AND ? AND TC.TRANSACTION_TYPE = 'deposit') - (SELECT SUM(TC.CREDIT) FROM UFABET.TRANSACTION_CREDIT TC WHERE  TC.CREATEDATE BETWEEN ? AND ? AND TC.TRANSACTION_TYPE = 'withdraw')),0) AS NET_PROFIT FROM DUAL",[tr_fdate,tr_edate ,tr_fdate,tr_edate,tr_fdate,tr_edate,tr_fdate,tr_edate]);
      result = await Database.raw("SELECT IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.CREATEDATE BETWEEN ? AND ? ),0) AS SUM_TRANSACTION_DEPOSIT, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.CREATEDATE BETWEEN ? AND ? ),0) AS SUM_TRANSACTION_WITHDAW, IFNULL(((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.CREATEDATE BETWEEN ? AND ? ) - (SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.CREATEDATE BETWEEN ? AND ? )),0) AS NET_PROFIT FROM DUAL",[tr_fdate,tr_edate ,tr_fdate,tr_edate,tr_fdate,tr_edate,tr_fdate,tr_edate]);
      status = 200
      message1 = query[0][0]
      message2 = result[0][0]
      return {status : status,
             credit : message1,
             trx:message2}
    }

}

module.exports = CreditController