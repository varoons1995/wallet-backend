'use strict'

const Database = use('Database')

class BankController {

    async FindAll(){
        const query = Database.table('BANK')
        const BankAll = await query
        // console.log(BankAll)
        return BankAll;
    }

}

module.exports = BankController
